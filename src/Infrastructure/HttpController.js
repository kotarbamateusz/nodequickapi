import CommandBus from './../Application/CommandBus.js'
import CreateEntityCommand from './../Application/Command/CreateEntityCommand.js'
import UpdateEntityCommand from './../Application/Command/UpdateEntityCommand.js'
import GetEntityCommand from './../Application/Command/GetEntityCommand.js'
import DeleteEntityCommand from './../Application/Command/DeleteEntityCommand.js'
import GetEntityListCommand from './../Application/Command/GetEntityListCommand.js'

export default class HttpController {
    constructor(commandBus) {
      this.commandBus = new CommandBus();
    }

    async getHealth(req, res) {
      return {status: 200, result: 'OK'};
    }

    async createEntity(req, res) {
      var command = new CreateEntityCommand(req.params.model, req.body);
      return await this.commandBus.execute(command);
    }

    async updateEntity(req, res) {
      var command = new UpdateEntityCommand(req.params.model, req.params.id, req.body);
      return await this.commandBus.execute(command);
    }

    async getEntity(req, res) {
      var command = new GetEntityCommand(req.params.model, req.params.id);
      return await this.commandBus.execute(command);
    }

    async deleteEntity(req, res) {
      var command = new DeleteEntityCommand(req.params.model, req.params.id);
      return await this.commandBus.execute(command);
    }

    async getList(req, res) {
      var command = new GetEntityListCommand(req.params.model, req.query);
      return await this.commandBus.execute(command);
    }
}