import Datastore from 'nedb';

export default class NeDBRepository {
    constructor(modelName) {
        this.db = new Datastore({filename: 'DB/'+modelName+'.db', autoload: true});
        this.db.loadDatabase();
    }

    async save(entity) {
        return new Promise((resolve, reject) => {
            return this.db.insert(entity, (err, doc) => {
                if (err) reject(err)
                else resolve(doc);
            });
        });
    }

    async update(id, entity) {
        return new Promise((resolve, reject) => {
            return this.db.update({_id: id}, entity, (err, doc) => {
                if (err) reject(err)
                else resolve(doc);
            });
        });
    }

    async get(id) {
        return new Promise((resolve, reject) => {
            return this.db.findOne({ _id: id}, function (err, doc) {
                if (err) reject(err)
                else resolve(doc);
            });
        });
    }

    async delete(id) {
        return new Promise((resolve, reject) => {
            return this.db.remove({ _id: id}, function (err, doc) {
                if (err) reject(err)
                else resolve(doc);
            });
        });
    }

    async count(parameters) {
        return new Promise((resolve, reject) => {
            return this.db.count(parameters, function (err, count) {
                if (err) reject(err)
                else resolve(count);
              });
        });
    }

    async list(parameters, from = 0, limit = 25) {
        return new Promise((resolve, reject) => {
            return this.db.find(parameters)
                .sort({ _id: 1 })
                .skip(from)
                .limit(limit).exec(function (err, docs) {
                    if (err) reject(err)
                    else resolve(docs);
              });
        });
    }
}