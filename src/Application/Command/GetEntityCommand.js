export default class GetEntityCommand {
    constructor(model, id) {
        this.model = model;
        this.id = id;
    }
}
