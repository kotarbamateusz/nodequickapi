export default class UpdateEntityCommand {
    constructor(model, id, data) {
        this.model = model;
        this.id = id;
        this.data = data;
    }
}
