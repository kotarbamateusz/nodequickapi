export default class CreateEntityCommand {
    constructor(model, data) {
        this.model = model;
        this.data = data;
    }
}
