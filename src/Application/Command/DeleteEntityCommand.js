export default class DeleteEntityCommand {
    constructor(model, id) {
        this.model = model;
        this.id = id;
    }
}
