export default class GetEntityListCommand {
    constructor(model, params) {
        this.model = model;

        this.from = params._from ? params._from : 0;
        if (params._from) delete params._from;

        this.limit = params._limit ? params._limit : 25;
        if (params._limit) delete params._limit;

        this.parameters = params;
    }
}
