export default class CommandBus {
    constructor() {
      this.handlers = {};
    }
  
    async execute(command) {
      let commandName = command.constructor.name;
      let handlerName = commandName.replace('Command', 'Handler');
      let handlerPath = `./../Application/Handler/${handlerName}.js`;
      let handler = await import(handlerPath);
      let handlerInstance = new handler.default();
      if (!handlerInstance) {
        throw new Error(`Handler ${handlerName} for command ${commandName} not found`);
      }

      return await handlerInstance.handle(command);
    }
  }