import HttpController from './../Infrastructure/HttpController.js'
import Config from './Config.js'
import express from 'express'
import Logger from './Services/Logger.js'
import ModelFactory from './Factory/ModelFactory.js'
import multer from 'multer';
import fs from 'fs';
import Repository from './../Infrastructure/Repository/NeDBRepository.js'

export default class Kernel {
    constructor() {
        this.config = new Config();
        this.logger = new Logger();
        this.controller = new HttpController();
        this.routes = this.config.getConfigJson('routes');
        this.app = express();
        this.app.use(express.json());
        // this.app.use(express.urlencoded({ extended: true }))
        this.entities = [];
      }

    async init() {
        this.initHttpEndpoints();
    }

    async initHttpEndpoints() {
        await this.initStorageEndpoints();
        await this.initConfigEndpoints();
        const port = process.env.PORT || 3000;
        this.app.listen(port);
        this.logger.debug(`Server running on port ${port}`);
    }

    async initConfigEndpoints() {
        var self = this;
        for (let i = 0; i<this.routes.length; i++) {
            let route = this.routes[i];
            if (typeof this.app[route.method] === 'function') {
                const controller = this.controller;
                const logger = this.logger;
                logger.debug(`Enable enpoint ${route.method} ${route.endpoint}`);
                this.app[route.method](route.endpoint, async function (req, res) {
                    logger.debug(`Enpoint ${route.method} ${route.endpoint}`);
                    var data = await controller[route.action](req, res);
                    res.status(data.status).json(data.result);
                });
            } else {
                console.log(`${method} method does not exist on this app`);
            }
        }
    }

    async initStorageEndpoints() {
        // storage
        const storage = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, 'Files/')
            },
            filename: function (req, file, cb) {
                var number = Date.now();
                var filename = file.fieldname+'-'+number+'.'+file.originalname.split('.').pop();
                cb(null, filename)
            }
        });
        const upload = multer({ storage: storage });
        this.logger.debug(`Enable enpoint /file/upload`);
        const repository = new Repository('file');
        this.app.post('/file/upload', upload.single('file'), (req, res) => {
            if (req.file) {
                repository.save(req.file)
                    .then((entity) => {
                        res.status(201).json({"id": entity._id});
                    })
            } else {
                res.status(400).json({ message: 'No file uploaded' });
            }
        });

        this.logger.debug(`Enable enpoint /file/content/:id`);
        this.app.get('/file/content/:id', async (req, res) => {
            var fileData = await repository.get(req.params.id);
            if (!fileData) {
                res.status(404).json({});
            } else {
                var filePath = fileData.path;
                var fileName = fileData.filename;
                var mimeType = fileData.mimetype;
                res.setHeader('Content-Disposition', `attachment; filename=${fileName}`);
                res.setHeader('Content-Type', mimeType);

                const fileStream = fs.createReadStream(filePath);
                fileStream.pipe(res);
            }
        });
    }

    async splitNumberIntoChunks(number) {
        const numberStr = number.toString();
        // Regular expression to match up to 3 digits from the end of the string
        return numberStr.match(/\d{1,5}(?=(\d{5})*$)/g) || [];
    }   

    async loadModels() {
        const config = this.config;
        var entityConfigList = config.getEntitiesConfigList();
        this.entities = entityConfigList.map((entityConfig) => {
            var entityConfig = config.getEntityConfig(entityConfig);
            var modelFactory = new ModelFactory();

            return modelFactory.init(entityConfig);
        });
    }

    async getModel(entityName) {
        await this.loadModels();
        return this.entities.find((entity) => {
            return entity.name == entityName;
        })
    }
}