export default class EntityFactory {
    constructor(model) {
        this.model = model;
        this.blockedFields = ['createdAt', 'entity'];
    }

    createFromPayload(payload) {
        if (this.model.fields) {
            var object = new Object();
            object.createdAt = (new Date()).toISOString();
            this.model.fields.map((field) => {
                var fieldName = field.data.name;
                if (this.blockedFields.includes(fieldName)) {
                    throw new Error('Fields: ["'+this.blockedFields.join('","')+'"] cannot be used in config!');
                }
                var payloadField = payload[fieldName];
                object[fieldName] = this.createData(field.data, payloadField);
            });

            return object;
        }
    }

    createData(configField, payloadField) {
        switch (configField.type) {
            case 'Choice': return this.createChoice(configField, payloadField);
            case 'Text': return payloadField;
            case 'Number': return parseInt(payloadField);
            case 'Float': return parseFloat(payloadField);
            default: throw new Error('Type "'+type+'" is undefined in "'+this.entityConfig.name+'" entity'); 
        }
    }

    createChoice(configField, payloadField) {
        var options = configField.options;
        if (!options || !options.includes(payloadField)) {
            var fieldName = configField.name;
            var choices = options.join(', ');
            throw new Error(`Invalid choice for field '${fieldName}'! Available: [${choices}]`);
        }

        return payloadField;
    }
}