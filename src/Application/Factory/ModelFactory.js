import Field from '../../Domain/Field.js'
import Choice from '../../Domain/Type/Choice.js'
import Text from '../../Domain/Type/Text.js'
import CreatedAt from '../../Domain/Type/CreatedAt.js'
import Number from '../../Domain/Type/Number.js'
import Float from '../../Domain/Type/Float.js'

export default class ModelFactory {
    init(entityConfig) {
        this.entityConfig = entityConfig;
        var fields = this.initFields(entityConfig.fields);

        return {
            name: entityConfig.name,
            fields: fields
        };
    }

    initFields(fields) {
        return fields.map((field) => {
            let type = this.getFieldByType(field.type);

            return new Field(field, type);
        })
    }

    getFieldByType(type) {
        switch (type) {
            case 'Choice': return new Choice(type);
            case 'Text': return new Text(type);
            case 'CreatedAt': return new CreatedAt(type);
            case 'Number': return new Number(type);
            case 'Float': return new Float(type);
            default: throw new Error('Type "'+type+'" is undefined in "'+this.entityConfig.name+'" entity'); 
        }
    }
}