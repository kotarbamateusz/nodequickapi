import chalk from 'chalk'

export default class Logger {
    constructor() {
        this.enabled = true;
    }

    async debug(msg) {
        if (this.enabled) {
            console.log(chalk.green(msg));
        }
    }

    async info(msg) {
        if (this.enabled) {
            console.log(chalk.blue(msg));
        }
    }

    async error(msg) {
        if (this.enabled) {
            console.log(chalk.red(msg));
        }
    }
}