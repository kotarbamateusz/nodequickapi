import EntityFactory from '../Factory/EntityFactory.js';
import Kernel from '../Kernel.js';
import Repository from './../../Infrastructure/Repository/NeDBRepository.js'

export default class CreateEntityHandler {
    constructor() {
        this.kernel = new Kernel;
    }

    async handle(command) {
        var modelName = command.model;
        var model = await this.kernel.getModel(modelName);
        var entityFactory = new EntityFactory(model)
        var model = entityFactory.createFromPayload(command.data);
        var repository = new Repository(modelName);
        var save = await repository.save(model);

        return {
            status: 201, 
            result: {"id": save._id}
        };
    }
}