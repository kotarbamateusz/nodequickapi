import Kernel from '../Kernel.js';
import Repository from '../../Infrastructure/Repository/NeDBRepository.js'

export default class DeleteEntityHandler {
    constructor() {
        this.kernel = new Kernel;
    }

    async handle(command) {
        var repository = new Repository(command.model);
        await repository.delete(command.id);
        
        return {status: 204, result: null};
    }
}