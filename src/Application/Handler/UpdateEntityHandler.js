import EntityFactory from '../Factory/EntityFactory.js';
import Kernel from '../Kernel.js';
import Repository from '../../Infrastructure/Repository/NeDBRepository.js'

export default class UpdateEntityHandler {
    constructor() {
        this.kernel = new Kernel;
    }

    async handle(command) {
        var modelName = command.model;
        var model = await this.kernel.getModel(modelName);
        var entityFactory = new EntityFactory(model)
        var model = entityFactory.createFromPayload(command.data);
        var repository = new Repository(modelName);
        var oldEntity = await repository.get(command.id);
        model.createdAt = oldEntity.createdAt;
        await repository.update(command.id, model);

        return {
            status: 200, 
            result: {"id": command.id}
        };
    }
}