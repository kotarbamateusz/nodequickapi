import Kernel from '../Kernel.js';
import Repository from '../../Infrastructure/Repository/NeDBRepository.js'

export default class GetEntityListHandler {
    constructor() {
        this.kernel = new Kernel;
    }

    async handle(command) {
        var repository = new Repository(command.model);
        var entity = await repository.list(command.parameters, command.from, command.limit);
        var count = await repository.count(command.parameters, command.from, command.limit);
        
        return {
            status: 200, 
            result: entity
        };
    }
}