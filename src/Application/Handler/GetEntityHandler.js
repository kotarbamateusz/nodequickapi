import Kernel from '../Kernel.js';
import Repository from '../../Infrastructure/Repository/NeDBRepository.js'

export default class GetEntityHandler {
    constructor() {
        this.kernel = new Kernel;
    }

    async handle(command) {
        var repository = new Repository(command.model);
        var entity = await repository.get(command.id);
        if (!entity) {
            return [404, null];
        }
        
        return {status: 200, result: entity};
    }
}