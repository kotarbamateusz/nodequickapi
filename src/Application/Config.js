import fs from 'fs'

export default class Config {
    getConfigJson(name) {
        let jsonString = fs.readFileSync(`./src/Config/${name}.json`).toString();
        return JSON.parse(jsonString);
    }

    getEntitiesConfigList() {
        return fs.readdirSync('./src/Domain/Entity/');
    }

    getEntityConfig(name) {
        let jsonString = fs.readFileSync(`./src/Domain/Entity/${name}`).toString();
        return JSON.parse(jsonString);
    }
}