export default class Choice {
    construct(options, data) {
        this.options = options;
    }

    validate() {
        return this.options.includes(data);
    }
}