export default class CreatedAt {
    construct(options, data) {
        this.options = options;
    }

    validate() {
        return this.options.includes(data);
    }
}