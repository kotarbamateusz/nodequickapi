export default class Entity {
    constructor(
        webhooks,
        fields
    ) {
        this.webhooks = webhooks;
        this.fields = fields;
      }

    getFields() {
        return this.fields;
    }
}