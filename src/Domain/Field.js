export default class Field {
    constructor(
        data,
        type
    ) {
        this.data = data;
        this.type = type;
      }

    getData() {
        return this.data;
    }

    getType() {
        return this.type;
    }
}