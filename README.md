## Getting started

[EARLY BETA] The idea of the project is to setup REST API in Nodejs in 5 minutes. No need to connect to external DB or storage. Enpoints for upload files and get file contents are instantly implemented. You can create new entity with full CRUD in 1 minute. It has no auth, so for now production use is not recommended.

**Use cases:**
-Rest API for local project
-Mock tests - easy create and fill objects
-automatics, logs
-AI memory

Setup
1. Clone this repo
2. Execute `npm install`
3. Add own entities in JSON
4. `bun start` / `npm start` (default port 3000, can be changed `PORT=8000 bun server.js`)

**How to add own entity?**
1. Go to directory /src/Domain/Entity/ and duplicate memory.json
2. Change filename eg: `news.json`, name `news` and modify `fields` in the JSON - available types [Text, Number, Float, Choice(options)]
* `_id` and `createdAt` are added to every entity
3. Run or restart application
4. This will create JSON endponts for you:
* GET /entity/news/list - get list of news
* GET /entity/news/:id - get news
* POST /entity/news - create news
* PUT /entity/news/:id - update news
* DELETE /entity/news/:id - delete news

Storage endpoints:
* POST /file/upload - upload Form-data single `file`
* GET /file/content/:id - get content of uploaded file
* GET /entity/file/:id - get uploaded file metadata
* GET /entity/file/list - get list of files metadata


**Concepts:**
- extremaly fast and easy CRUD
- create fast entities, just copy and edit JSON with models
- simple field types
- files stored in local directory - no need to configure anything
- easy file management, enpoint for upload files - stored in DB with metadata
- enpoint to get uploaded file contents
- good response time (it doesn't have to be epic, it should work nice)
- it should be stable and it should work with average traffic on average site (it would be ideal to handle 1000 req/s)
For the first shot with local DB I will try NeDB
```
Insert: 10,680 ops/s
Find: 43,290 ops/s
Update: 8,000 ops/s
Remove: 11,750 ops/s
```

Future concepts and TODOS:
-add auth on PUT/POST/DELETE (manage only with permissions) - maybe add users
-dont close app on error
-add tests
-validation
-cached views with multiple dataset
