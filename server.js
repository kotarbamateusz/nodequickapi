import Kernel from './src/Application/Kernel.js'

const kernel = new Kernel();
kernel.init();